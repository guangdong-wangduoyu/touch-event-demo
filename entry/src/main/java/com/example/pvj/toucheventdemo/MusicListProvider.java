package com.example.pvj.toucheventdemo;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.example.pvj.toucheventdemo.bean.MusicData;

import java.util.List;

public class MusicListProvider extends BaseItemProvider {
    // ListContainer的数据集合
    private List<MusicData> lists;
    private Context slice;
    private int curSelectPosition = 0;
    private final Color selectColor = new Color(Color.getIntColor("#f84c4b"));
    private final Color defColor = new Color(Color.getIntColor("#111111"));
    private final Color isPlay = new Color(Color.getIntColor("#c8c8c8"));


    public MusicListProvider(List<MusicData> list, Context slice) {
        this.lists = list;
        this.slice = slice;

    }

    @Override
    public int getCount() {
        return lists == null ? 0 : lists.size();
    }

    @Override
    public MusicData getItem(int position) {
        if (lists != null && position >= 0 && position < lists.size()) {
            return lists.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        Holder holder;
        MusicData data = lists.get(position);
        if (component == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_music, null, false);
            holder = new Holder(cpt);
            // 将获取到的子组件信息绑定到列表项的实例中
            cpt.setTag(holder);
        } else {
            cpt = component;
            // 从缓存中获取到列表项实例后，直接使用绑定的子组件信息进行数据填充。
            holder = (Holder) cpt.getTag();

        }


        if (data.index <= 3 || curSelectPosition == position) {
            holder.indexText.setTextColor(Color.RED);
        } else {
            holder.indexText.setTextColor(Color.DKGRAY);
        }

        if (curSelectPosition == position){
            holder.titleText.setTextColor(Color.RED);
            holder.singText.setTextColor(Color.RED);
        }else {
            holder.titleText.setTextColor(Color.BLACK);
            holder.singText.setTextColor(Color.DKGRAY);
        }

        holder.musicImg.setVisibility(curSelectPosition == position
            ? Component.VISIBLE : Component.HIDE
        );

        holder.indexText.setText(data.index + "");
        holder.titleText.setText(data.title);
        holder.singText.setText(data.singer + "-" + data.subName);
        return cpt;
    }

    // 用于保存列表项中的子组件信息
    public static class Holder {
        Image musicImg;
        Text indexText;
        Text titleText;
        Text singText;

        Holder(Component component) {
            indexText = component.findComponentById(ResourceTable.Id_tv_index);
            musicImg = component.findComponentById(ResourceTable.Id_img_music);
            titleText = component.findComponentById(ResourceTable.Id_tv_title);
            singText = component.findComponentById(ResourceTable.Id_tv_sing);
        }
    }


    public int getCurSelectPosition() {
        return curSelectPosition;
    }

    public void setCurSelectPosition(int curSelectPosition) {
        this.curSelectPosition = curSelectPosition;
        notifyDataChanged();
    }
}

