/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.pvj.toucheventdemo;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

/**
 * @since 2021-11-25
 */
public class MyListContainer  extends ListContainer {
    public MyListContainer(Context context) {
        super(context);
    }

    public MyListContainer(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }


    @Override
    public void setTouchEventListener(TouchEventListener listener) {
        super.setTouchEventListener(listener);
    }
}
